﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Facebook;

namespace FacebookWebForm
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //token is the thing that very importance
            var client = new FacebookClient(FbHelper.Token);

            var fql =
                @"SELECT uid, 
                    name, 
                    pic,
                    pic_square, 
                    profile_url 
                    FROM user WHERE uid in 
                    (SELECT uid2 FROM friend 
                    WHERE uid1 = me())
                    ORDER BY name";
//                    LIMIT 0, 1";
            dynamic query = client.Query(fql);


            string path = HttpContext.Current.Server.MapPath("~/App_Data/PostedFriendIds.txt");
           var postedFacebookIds =  File.ReadAllLines(path).Select(i => Convert.ToInt64(i));


            var list = new List<Friend>();
            foreach (var obj in query)
            {
                var facebookId = Convert.ToInt64(obj.uid);
                list.Add(new Friend()
                {
                    
                    id =facebookId ,
                    name = obj.name.ToString(),
                    pic_square = obj.pic.ToString(),
                    profile_url =
                    obj.profile_url.ToString(),
                    is_posted  = postedFacebookIds.Any(i => i == facebookId)
                });
            }

            GridView1.DataSource = list.OrderBy(p => p.name);
            GridView1.DataBind();
        }


    }
}
