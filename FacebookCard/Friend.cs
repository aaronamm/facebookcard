﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FacebookWebForm
{
    public class Friend 
    {
        public long id { get; set; }
        public string name { get; set; }
        public string pic_square { get; set; }
        public string profile_url { get; set; }
        public bool is_posted { get; set; }
    }
}