﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace FacebookWebForm
{
    public class FbHelper
    {
      
        public static string Token
        {
            get { return HttpContext.Current.Session["token"]
                .ToString(); }

            set { HttpContext.Current.Session["token"] = value; }
        }
    }
}