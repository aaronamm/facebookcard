﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="FacebookWebForm._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <p>
        <input type="button" value="login with your Facebook" id="btnFbLogin" />
        <asp:Button ID="btnReloadFriend" runat="server" Text="load all friends" OnClick="Button1_Click" />
    </p>
    card url
    <input type="text" id="cardImageUrl" value="http://images.all-free-download.com/images/graphiclarge/happy_new_year_2014_266943.jpg"
        style="font-size: 14px; width: 800px;" />
    <div style="height: 450px; overflow: auto">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="800px">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="Facebook Id" />
                <asp:BoundField DataField="name" HeaderText="Name" />
                <asp:ImageField DataImageUrlField="pic_square" HeaderText="Photo">
                </asp:ImageField>
                <asp:HyperLinkField DataTextField="profile_url" HeaderText="Facebook Url" Target="_blank"
                    DataNavigateUrlFields="profile_url" />
                <asp:HyperLinkField HeaderText="Post to Friend wall" Text="Post" NavigateUrl="#"
                    ControlStyle-CssClass="profileUrl" />
                <asp:CheckBoxField DataField="is_posted" ControlStyle-CssClass="isPosted" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
