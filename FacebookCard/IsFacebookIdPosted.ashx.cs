﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FacebookWebForm
{
    /// <summary>
    /// Summary description for IsFacebookIdPosted
    /// </summary>
    public class IsFacebookIdPosted : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            var facebookId = Convert.ToInt64(context.Request.Form["facebookId"]);
            string path = HttpContext.Current.Server.MapPath("~/App_Data/PostedFriendIds.txt");
            var postedFacebookIds = File.ReadAllLines(path).Select(i => Convert.ToInt64(i));

            var isPosted = postedFacebookIds.Any(i => i == facebookId);
            context.Response.ContentType = "application/json";
            context.Response.Write(string.Format("{{isPosted:{0}}}",isPosted.ToString().ToLower()));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}