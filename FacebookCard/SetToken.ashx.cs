﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace FacebookWebForm
{
    /// <summary>
    /// Summary description for SetToken
    /// </summary>
    public class SetToken : IHttpHandler, IRequiresSessionState 
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            var token = context.Request.Form["token"];
            FbHelper.Token = token;
            //context.Response.Redirect("/");
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}