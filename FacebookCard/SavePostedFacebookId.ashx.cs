﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FacebookWebForm
{
    /// <summary>
    /// Summary description for SavePostedFacebookId
    /// </summary>
    public class SavePostedFacebookId : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var facebookId = context.Request.Form["facebookId"];
            string path = HttpContext.Current.Server.MapPath("~/App_Data/PostedFriendIds.txt");
            using (var fs = new FileStream(path, FileMode.Append, FileAccess.Write))
            using(var sw = new StreamWriter(fs))
            {
                sw.WriteLine(facebookId);
            }
            context.Response.ContentType = "application/json";
            context.Response.Write("ok");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}